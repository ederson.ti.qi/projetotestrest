package methods;

import com.github.javafaker.Faker;
import dto.BookingDTO;
import org.junit.jupiter.api.DisplayName;

import java.time.LocalDate;

public class MethodsBooking {

    private static Faker faker = new Faker();

    @DisplayName("Criar uma Reserva")

    public static BookingDTO CreateBooking(){
        LocalDate dateActual = LocalDate.now();

        return new BookingDTO (
                faker.name().firstName(),
                faker.name().lastName(),
                faker.number().numberBetween(1, 900),
                true,
                new BookingDTO.BookingDatesDTO(
                        dateActual.toString(),
                        dateActual.plusDays(2).toString()
                ),
                "Breakfast"

        );
    }

    public static BookingDTO CreateBookingFirtNameNull(){
        LocalDate dateActual = LocalDate.now();

        return new BookingDTO (
                null,
                null,
                11,
                true,
                new BookingDTO.BookingDatesDTO(
                        null,
                       null
                ),
                "Breakfast"

        );
    }
}
