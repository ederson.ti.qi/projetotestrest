package base;

import dto.BookingDTO;
import dto.ResponseBookingDTO;
import methods.MethodsBooking;
import org.junit.jupiter.api.BeforeEach;


public class BaseTest {

    public ResponseBookingDTO responseBookingDTO;
    public BookingDTO bookingRequest;
    public MethodsBooking methodsBooking;


    @BeforeEach
    public void baseSetUp() {
        methodsBooking = new MethodsBooking();
    }
}
