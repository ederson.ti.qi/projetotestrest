package tests;

import base.BaseTest;
import dto.ResponseBookingDTO;
import methods.MethodsLodin;
import org.apache.http.HttpStatus;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import service.RestService;

import static util.staticValue.PATH;

public class CreateBooking2 extends BaseTest {

    @Test
    @DisplayName("Cadastra uma reserva e valida os dados")
    void createBookingSuccess() {
        bookingRequest = methodsBooking.CreateBooking();

        MethodsLodin methodsLodin = new MethodsLodin();

        responseBookingDTO = RestService.post(bookingRequest, PATH)
                .then().statusCode(HttpStatus.SC_OK)
                .extract()
                .response().getBody().as(ResponseBookingDTO.class);

        Assertions.assertAll(
                "Valida dados do livro criado",
                () -> Assertions.assertEquals(bookingRequest.firstname(), responseBookingDTO.booking().firstname()),
                () -> Assertions.assertEquals(bookingRequest.lastname(),  responseBookingDTO.booking().lastname()),
                () -> Assertions.assertEquals(bookingRequest.totalprice(),responseBookingDTO.booking().totalprice()),
                () -> Assertions.assertEquals(bookingRequest.depositpaid(), responseBookingDTO.booking().depositpaid()),
                () -> Assertions.assertEquals(bookingRequest.bookingdates().checkin() , responseBookingDTO.booking().bookingdates().checkin()),
                () -> Assertions.assertEquals(bookingRequest.bookingdates().checkout(), responseBookingDTO.booking().bookingdates().checkout()),
                () -> Assertions.assertEquals(bookingRequest.additionalneeds(), responseBookingDTO.booking().additionalneeds())
        );
    }

    @Test
    @DisplayName("Cadastra uma reserva sem body")
    void createBookingNoBody() {
        RestService.post(null, PATH)
                .then()
                .statusCode(HttpStatus.SC_BAD_REQUEST).extract().response();
    }

}
