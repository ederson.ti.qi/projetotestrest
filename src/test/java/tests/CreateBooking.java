package tests;


import base.BaseTest;
import dto.BookingDTO;
import dto.ResponseBookingDTO;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import methods.MethodsBooking;
import org.apache.http.HttpStatus;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import service.RestService;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static util.staticValue.PATH;

public class CreateBooking extends BaseTest {


    @Test
    @DisplayName("Cadastra uma reserva e valida os dados")
    void createBookingSuccess() {

        BookingDTO bookingRequest = MethodsBooking.CreateBooking();

        Response response = RestService.post(bookingRequest, "/booking");

        assertEquals(HttpStatus.SC_OK, response.statusCode());

        JsonPath jsonResponse = response.jsonPath();

        assertEquals(bookingRequest.firstname(), jsonResponse.getString("booking.firstname"));
        assertEquals(bookingRequest.lastname(), jsonResponse.getString("booking.lastname"));
        assertEquals(bookingRequest.totalprice(), jsonResponse.getInt("booking.totalprice"));
        assertEquals(bookingRequest.depositpaid(), jsonResponse.getBoolean("booking.depositpaid"));
        assertEquals(bookingRequest.bookingdates().checkin(), jsonResponse.getString("booking.bookingdates.checkin"));
        assertEquals(bookingRequest.bookingdates().checkout(), jsonResponse.getString("booking.bookingdates.checkout"));
        assertEquals(bookingRequest.additionalneeds(), jsonResponse.getString("booking.additionalneeds"));
    }

    @Test
    @DisplayName("Cadastra uma reserva e valida os dados")
    void createBookingSuccess3() {

        bookingRequest = methodsBooking.CreateBooking();

        responseBookingDTO = RestService.post(bookingRequest, PATH)
                .then()
                .statusCode(HttpStatus.SC_OK)
                .extract()
                .response().getBody().as(ResponseBookingDTO.class);

        Assertions.assertAll(
                "Valida dados do livro criado",
                () -> Assertions.assertNotEquals(null, responseBookingDTO.booking().firstname()),
                () -> Assertions.assertEquals(bookingRequest.firstname(), responseBookingDTO.booking().firstname()),
                () -> Assertions.assertEquals(bookingRequest.lastname(),  responseBookingDTO.booking().lastname()),
                () -> Assertions.assertEquals(bookingRequest.totalprice(),responseBookingDTO.booking().totalprice()),
                () -> Assertions.assertEquals(bookingRequest.depositpaid(), responseBookingDTO.booking().depositpaid()),
                () -> Assertions.assertEquals(bookingRequest.bookingdates().checkin(), responseBookingDTO.booking().bookingdates().checkin()),
                () -> Assertions.assertEquals(bookingRequest.bookingdates().checkout(), responseBookingDTO.booking().bookingdates().checkout()),
                () -> Assertions.assertEquals(bookingRequest.additionalneeds(), responseBookingDTO.booking().additionalneeds())
        );
    }

    @Test
    @DisplayName("Cadastra uma reserva sem body")
    void createBookingNoBody() {
        Response response = RestService.post(null, "/booking");
        assertEquals(HttpStatus.SC_BAD_REQUEST, response.statusCode());
    }

}
